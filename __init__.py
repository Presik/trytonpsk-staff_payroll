#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import period
from . import wage_type
from . import payroll
from . import category
from . import employee
from . import configuration
from . import position


def register():
    Pool.register(
        position.Position,
        position.WorkdayDefinition,
        period.Period,
        wage_type.WageType,
        employee.MandatoryWage,
        category.EmployeeCategory,
        employee.Employee,
        payroll.Payroll,
        payroll.PayrollLine,
        category.CategoryWagesDefault,
        payroll.PayrollGroupStart,
        configuration.StaffConfigurationSequence,
        configuration.StaffConfiguration,
        wage_type.WageTypeSalary,
        payroll.Move,
        module='staff_payroll', type_='model')
    Pool.register(
        payroll.PayrollReport,
        module='staff_payroll', type_='report')
    Pool.register(
        period.OpenPeriod,
        payroll.PayrollGroup,
        employee.CreateMandatoryWages,
        payroll.PayrollPreliquidation,
        payroll.PayrollRecompute,
        module='staff_payroll', type_='wizard')
